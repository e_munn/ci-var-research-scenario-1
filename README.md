# 👋 Hi
Thank you for participating in the research to evaluate the GitLab CI/CD variables offerings. The instructions for the activity are as following:

## 📝 Instructions

In this scenario, you are a DevOps engineer responsible for two pipelines:

- The pipeline in this project.
- The pipeline in a [downstream project](https://gitlab.com/ci-research-2021-may/ci-var-research-scenario-1-b)
  that auto-generates API docs for this project.

You want to regenerate the API docs every time there is a release in the main project. You also want to make sure the auto-generated docs are tagged with the same version as the release.

## ℹ️ Documentation links
Read more about:
1. [Multiproject pipeline variables](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html#passing-cicd-variables-to-a-downstream-pipeline)
2. [Predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
3. [CI/CD variable precedence](https://docs.gitlab.com/ee/ci/variables/README.html#cicd-variable-precedence)





